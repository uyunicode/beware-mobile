import 'package:flutter/material.dart';

import 'package:beware_mobile_app/src/pages/login/widgets/login_form.dart';
import 'package:beware_mobile_app/src/pages/login/widgets/background_login.dart';
import 'package:flutter_svg/svg.dart';

class Login extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _size = MediaQuery.of(context).size;
    return Scaffold(
        body: Stack(
      children: <Widget>[
        BackgroundLogin(),
        SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.fromLTRB(0, 80, 0, 40),
                child: SvgPicture.asset(
                  'assets/icons/login.svg',
                  height: _size.height * 0.30,
                  width: _size.width * 0.30,
                ),
              ),
              Container(
                padding: EdgeInsets.fromLTRB(35, 0, 35, 0),
                child: LoginForm(),
              ),
              _signupButton(context),
              _forgotPasswordButton(context)
            ],
          ),
        )
      ],
    ));
  }

  Widget _signupButton(BuildContext context) {
    return TextButton(
      child: Text(
        "¿No tienes una Cuenta? Registrate",
        style: TextStyle(color: Colors.deepPurple),
      ),
      onPressed: () => Navigator.of(context).pushNamedAndRemoveUntil('signup', (Route<dynamic> route) => true),
    );
  }

  Widget _forgotPasswordButton(BuildContext context) {
    return TextButton(
      child: Text(
        "¿Olvidaste tu contraseña?",
        style: TextStyle(color: Colors.deepPurple),
      ),
      onPressed: () => Navigator.of(context).pushNamedAndRemoveUntil('forgot_password', (Route<dynamic> route) => true),
    );
  }
}
