import 'dart:math';

import 'package:flutter/material.dart';

class BackgroundLogin extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _gradient = Container(
      width: double.infinity,
      height: double.infinity,
      decoration: BoxDecoration(
          gradient: LinearGradient(
              colors: [Color(0xECE9E6), Color(0xFFFFFF)],
              begin: FractionalOffset(0.0, 0.6),
              end: FractionalOffset(0.0, 1.0))),
    );
    // Monrise #DAE2F8, #D6A4A4
    final _figure = Transform.rotate(
        angle: -pi / 5.0,
        child: Container(
            width: 200.0,
            height: 200.0,
            decoration: BoxDecoration(
                gradient: LinearGradient(colors: [
                  Color.fromRGBO(230, 190, 236, 1.0),
                  Color.fromRGBO(201, 241, 244, 1.0)
                ]),
                borderRadius: BorderRadius.circular(90.0))));
    return Stack(children: <Widget>[
      _gradient,
      Positioned(top: -100, left: -80, child: _figure),
    ]);
  }
}
