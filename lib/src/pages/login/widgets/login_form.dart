import 'package:beware_mobile_app/src/providers/intrafamily_violence_test_provider.dart';
import 'package:beware_mobile_app/src/providers/socket_service.dart';
import 'package:beware_mobile_app/src/providers/trusted_person.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:fluttertoast/fluttertoast.dart';
import 'package:beware_mobile_app/src/pages/login/login_validator.dart';
import 'package:beware_mobile_app/src/providers/user_provider.dart';
import 'package:beware_mobile_app/src/services/HttpServer.dart';

class LoginForm extends StatefulWidget {
  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final _formKey = GlobalKey<FormState>();
  String _password = "";
  bool _obscurePasswordState = true;
  @override
  Widget build(BuildContext context) {
    final UserProvider userProvider = Provider.of<UserProvider>(context);
    final SocketService socketService = Provider.of<SocketService>(context);
    final TrustedPersonProvider trustedPersonProvider =
        Provider.of<TrustedPersonProvider>(context);
    final IntrafamilyViolenceTestProvider intraFamilyViolenceTestProvider =
        Provider.of<IntrafamilyViolenceTestProvider>(context);
    return Form(
        key: _formKey,
        child: Column(
          children: <Widget>[
            _userTextFormField(userProvider),
            SizedBox(
              height: 15,
            ),
            _passwordTextFormField(),
            SizedBox(
              height: 15,
            ),
            _loginButton(context, userProvider, socketService,
                trustedPersonProvider, intraFamilyViolenceTestProvider)
          ],
        ));
  }

  Widget _loginButton(
      BuildContext context,
      UserProvider userProvider,
      SocketService socketService,
      TrustedPersonProvider trustedPersonProvider,
      IntrafamilyViolenceTestProvider intrafamilyViolenceTestProvider) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
          primary: Colors.deepPurple,
          onPrimary: Colors.white,
          shape: StadiumBorder(),
          fixedSize: Size(290, 40)),
      onPressed: () => _loginRequest(context, userProvider, socketService,
          trustedPersonProvider, intrafamilyViolenceTestProvider),
      child: Text("LOGIN"),
    );
  }

  Future<void> _loginRequest(
      BuildContext context,
      UserProvider userProvider,
      SocketService socketService,
      TrustedPersonProvider trustedPersonProvider,
      IntrafamilyViolenceTestProvider intrafamilyViolenceTestProvider) async {
    if (_formKey.currentState!.validate()) {
      HttpServer httpServer = HttpServer();
      Map response = await httpServer.post(
          "/user_management/possible_victim_manage/sign_in",
          {"email": userProvider.email, "password": _password});

      if (response['msg'] == "session success") {
        await _setUserProvider(userProvider);
        await UserProvider.writeToken(response['jwt']);
        await socketService.connect();
        await _setTrustedPersonProvider(userProvider.ci, trustedPersonProvider);
        await _setIntrafamilyViolenceTestProvider(
            intrafamilyViolenceTestProvider);
        Navigator.of(context)
            .pushNamedAndRemoveUntil('home', (Route<dynamic> route) => true);
      } else if (response['msg'] == "password incorrect")
        _showMessage("Contraseña Incorrecta!");
      else if (response['msg'] == "account not found")
        _showMessage("La Cuenta No Existe!");
      else
        _showMessage("Error, Intente nuevamente!");
    }
  }

  _setUserProvider(UserProvider userProvider) async {
    HttpServer httpServer = HttpServer();
    Map response = await httpServer
        .get("/user_management/possible_victim_manage/${userProvider.email}");
    if (response['status'] == 200) {
      response = response['data'];
      userProvider.setAll(
          response['ci'],
          response['name'],
          response['cellphone'],
          response['email'],
          response['nickname'],
          response['sex'],
          response['birthday'],
          response['civil_status'],
          response['address'],
          response['profile']);
    }
  }

  Future<void> _setTrustedPersonProvider(
      int ci, TrustedPersonProvider trustedPersonProvider) async {
    HttpServer httpServer = HttpServer();
    Map response = await httpServer.get(
        "/user_management/possible_victim_manage/get_list_trusted_person/$ci");
    trustedPersonProvider.setlistPersonOfUser(response['data']['data']);
  }

  Future<void> _setIntrafamilyViolenceTestProvider(
      IntrafamilyViolenceTestProvider intraFamilyViolenceTestProvider) async {
    HttpServer http = new HttpServer();
    Map response = await http.get(
        "/request_help_management/intrafamily_violence_test_manage/get_questions_test");
    if (response['status'] == 200) {
      intraFamilyViolenceTestProvider.questions = response['data'];
    }
  }

  _showMessage(String message) {
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 1,
        backgroundColor: Color.fromRGBO(230, 230, 243, 1),
        textColor: Colors.deepPurple,
        fontSize: 16.0);
  }

  InputDecoration _inputDecoration(
      String hintText, String labelText, bool icon) {
    Icon _iconInput = Icon(Icons.person, color: Colors.deepPurple);
    Widget _suffix = Text("");
    if (!icon) {
      _iconInput = Icon(Icons.password, color: Colors.deepPurple);
      _suffix = TextButton(
          onPressed: () => setState(() {
                _obscurePasswordState = !_obscurePasswordState;
              }),
          child: Icon(
            Icons.lock_open,
            color: Colors.deepPurple,
          ));
    }

    return InputDecoration(
        fillColor: Color.fromRGBO(249, 243, 255, 1.0),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20),
            borderSide: BorderSide(
              color: Color.fromRGBO(249, 243, 255, 1.0),
            )),
        focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20),
            borderSide: BorderSide(
              color: Color.fromRGBO(249, 255, 255, 1.0),
            )),
        filled: true,
        hintText: hintText,
        labelText: labelText,
        icon: _iconInput,
        suffixIcon: _suffix);
  }

  Widget _userTextFormField(UserProvider userProvider) {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      cursorColor: Colors.deepPurple,
      style: TextStyle(
        color: Color.fromRGBO(89, 88, 90, 1.0),
        fontStyle: FontStyle.italic,
      ),
      decoration: _inputDecoration("Escribe tu email", "Email", true),
      validator: (value) {
        if (value!.isEmpty) return 'Escribe tu email!';
        if (!LoginValidator.validateEmail(value))
          return 'Escribe un Correo Valido!';
      },
      onChanged: (value) {
        userProvider.email = value;
      },
    );
  }

  Widget _passwordTextFormField() {
    return TextFormField(
      keyboardType: TextInputType.visiblePassword,
      obscureText: _obscurePasswordState,
      decoration:
          _inputDecoration("Escribe Tu Contraseña", "Contraseña", false),
      validator: (value) {
        if (value!.isEmpty) return 'Escribe tu contraseña!';
        if (!LoginValidator.validatePassword(value))
          return 'Contraseña Incorrecta';
      },
      onChanged: (value) {
        _password = value;
      },
    );
  }
}
