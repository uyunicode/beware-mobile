import 'dart:ui';

import 'package:beware_mobile_app/src/providers/intrafamily_violence_test_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Question extends StatefulWidget {
  final int? questionNumber;
  Question({Key? key, @required this.questionNumber}) : super(key: key);

  @override
  _QuestionState createState() => _QuestionState();
}

class _QuestionState extends State<Question> {
  int _optionSelected = 1;
  int? _questionNumber;
  IntrafamilyViolenceTestProvider? _intrafamilyViolenceTestProvider;
  @override
  void initState() {
    super.initState();
    this._questionNumber = super.widget.questionNumber;
    _intrafamilyViolenceTestProvider =
        Provider.of<IntrafamilyViolenceTestProvider>(context, listen: false);
    this._optionSelected =
        this._intrafamilyViolenceTestProvider!.answers[this._questionNumber!];
  }

  @override
  Widget build(BuildContext context) {
    return Card(
        child: Padding(
      padding: EdgeInsets.all(8),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          ListTile(
            title: Text(
              '${this._questionNumber! + 1}. ${_intrafamilyViolenceTestProvider?.questions[this._questionNumber!]}',
              style: _getTextStyle(),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              _radioButton(1),
              SizedBox(
                width: 30,
              ),
              _radioButton(2),
              SizedBox(
                width: 30,
              ),
              _radioButton(3)
            ],
          ),
          Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            Text("Nunca",
                style: TextStyle(
                    color: Colors.deepPurple, fontWeight: FontWeight.bold)),
            SizedBox(
              width: 15,
            ),
            Text("En Ocasiones",
                style: TextStyle(
                    color: Colors.deepPurple, fontWeight: FontWeight.bold)),
            SizedBox(
              width: 15,
            ),
            Text("Casi Siempre",
                style: TextStyle(
                    color: Colors.deepPurple, fontWeight: FontWeight.bold))
          ])
        ],
      ),
    ));
  }

  Widget _radioButton(int value) {
    return Radio<int>(
      value: value,
      groupValue: this._optionSelected,
      activeColor: Colors.deepPurple,
      onChanged: (int? value) {
        setState(() {
          this._optionSelected = value!;
        });
        print("_optionSelected[${this._questionNumber}]: $_optionSelected");
        this
            ._intrafamilyViolenceTestProvider
            ?.setAnswer(this._optionSelected, this._questionNumber!);
      },
    );
  }

  TextStyle _getTextStyle() {
    Color textColor = Colors.green;
    if (this._questionNumber! > 9 && this._questionNumber! < 21)
      textColor = Colors.yellow[600]!;
    if (this._questionNumber! > 20) textColor = Colors.red;
    return TextStyle(
        color: textColor,
        fontSize: 18,
        fontStyle: FontStyle.italic,
        fontWeight: FontWeight.bold);
  }
}
