import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:beware_mobile_app/src/providers/intrafamily_violence_test_provider.dart';
import 'package:beware_mobile_app/src/pages/intrafamily_violence_test/widgets/question_page.dart';
import 'package:beware_mobile_app/src/services/HttpServer.dart';

class IntrafamilyViolenceTest extends StatefulWidget {
  IntrafamilyViolenceTest({Key? key}) : super(key: key);

  @override
  _IntrafamilyViolenceTestState createState() =>
      _IntrafamilyViolenceTestState();
}

class _IntrafamilyViolenceTestState extends State<IntrafamilyViolenceTest> {
  PageController _pageViewController = new PageController(initialPage: 0);
  final int _QUANTITY_QUESTIONS = 3;

  @override
  Widget build(BuildContext context) {
    return PageView(
      controller: _pageViewController,
      scrollDirection: Axis.vertical,
      children: [
        _pageBody(1),
        _pageBody(2),
        _pageBody(3),
        _pageBody(4),
        _pageBody(5),
        _pageBody(6),
        _pageBody(7),
        _pageBody(8),
        _pageBody(9),
        _pageBody(10),
        _pageBody(11)
      ],
    );
  }

  Widget _pageBody(int numberPage) {
    Widget button = _buttonNext();
    if (numberPage == 11) button = _buttonResult();
    return Padding(
      padding: EdgeInsets.fromLTRB(8, 30, 8, 8),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Question(
              questionNumber:
                  _QUANTITY_QUESTIONS * numberPage - _QUANTITY_QUESTIONS),
          Question(
              questionNumber:
                  _QUANTITY_QUESTIONS * numberPage - (_QUANTITY_QUESTIONS - 1)),
          Question(
              questionNumber:
                  _QUANTITY_QUESTIONS * numberPage - (_QUANTITY_QUESTIONS - 2)),
          button
        ],
      ),
    );
  }

  Widget _buttonNext() {
    return TextButton.icon(
      onPressed: () => _nextPage(),
      label: Text(
        "Siguiente",
        style: TextStyle(
            color: Colors.deepPurple,
            fontSize: 20.0,
            fontWeight: FontWeight.bold,
            fontStyle: FontStyle.italic),
      ),
      icon:
          Icon(Icons.keyboard_arrow_down, size: 50.0, color: Colors.deepPurple),
    );
  }

  Widget _buttonResult() {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
          primary: Colors.deepPurple,
          onPrimary: Colors.white,
          shape: StadiumBorder(),
          fixedSize: Size(290, 40)),
      onPressed: () async => await _generateResult(),
      child: Text("TERMINAR"),
    );
  }

  _nextPage() {
    _pageViewController.nextPage(
        duration: Duration(milliseconds: 800), curve: Curves.fastOutSlowIn);
  }

  Future<void> _generateResult() async {
    HttpServer httpClient = new HttpServer();
    final IntrafamilyViolenceTestProvider intrafamilyViolenceTestProvider =
        Provider.of<IntrafamilyViolenceTestProvider>(context, listen: false);
    Map response = await httpClient.post(
        "/request_help_management/intrafamily_violence_test_manage/test_answers",
        {'answers': intrafamilyViolenceTestProvider.answers});
    if (response['status'] == 200) {
      if (response['data'] == 0)
        _showResult(
            "Nos alegramos por ti, no eres victima de violencia intrafamiliar",
            response['data']);
      else if (response['data'] == 1) {
        //phase1
        _showResult(
            "Debes reconocer que te encuentras ya en las primeras fases del ciclo. Puedes informarte sobre lo que significa el Ciclo de la violencia y de qué modos puedes salir; siempre debes evitar aislarte de personas de tu confianza, y debes comprender que sufrir violencia no es un tema para avergonzarse, sino que al contrario, es una situación en la que se debe solicitar ayuda.",
            response['data']);
      } else if (response['data'] == 2) {
        _showResult(
            "Estás enfrentando un nivel más avanzado y visible de violencia. En este punto tu autoestima se verá afectada, te sentirás confundida y creerás que el maltrato es parte de una relación de afecto. Es complejo salir del Ciclo porque muchas veces pensamos que la persona agresora reacciona de esa manera por nuestra culpa o porque tuvo un pasado difícil, lo que nos hace crearnos la falsa expectativa de que nuestro amor y atención incondicionales pueden hacer que la persona agresora cambie. En este punto debemos prestar atención a todas las formas en las que puede manifestarse la violencia, como son la indiferencia afectiva, el control sobre nuestros bienes y sobre nuestra libertad personal. Podemos creer que renunciar a nuestros derechos es una muestra de amor a la otra persona, cuando realmente es una aceptación sumisa de la capacidad de dominación de la otra persona sobre nosotras.",
            response['data']);
      } else {
        _showResult(
            "Debes tener presente que tu vida corre un inminente peligro. Nunca es muy tarde para pedir ayuda.",
            response['data']);
      }
    }
  }

  Future<void> _showResult(String result, int numberPhase) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Resultado'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                numberPhase > 0
                    ? ListTile(
                        title: Text(
                          'Te encuentras en la Fase $numberPhase',
                          style: this._getTextStyle(numberPhase),
                        ),
                        leading: this._getLeading(numberPhase),
                      )
                    : ListTile(
                        title: Text(
                          'NO te encuentras en ninguna fase',
                          style: this._getTextStyle(numberPhase),
                        ),
                        leading: this._getLeading(numberPhase),
                      ),
                Text('$result'),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: const Text('Finalizar',
                  style: TextStyle(
                      color: Colors.deepPurple, fontWeight: FontWeight.bold)),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  TextStyle _getTextStyle(int numberPhase) {
    Color textColor = Colors.green;
    if (numberPhase == 2) textColor = Colors.yellow[600]!;
    if (numberPhase == 3) textColor = Colors.red;
    return TextStyle(
        color: textColor,
        fontSize: 18,
        fontStyle: FontStyle.italic,
        fontWeight: FontWeight.bold);
  }

  Icon _getLeading(int numberPhase) {
    Icon icon = Icon(
      Icons.check,
      color: Colors.green,
      size: 30,
    );
    if (numberPhase == 1)
      icon = Icon(
        Icons.warning,
        color: Colors.green,
        size: 30,
      );
    else if (numberPhase == 2)
      icon = Icon(
        Icons.warning,
        color: Colors.yellow[600],
        size: 30,
      );
    else if (numberPhase == 3)
      icon = Icon(
        Icons.dangerous,
        color: Colors.red,
        size: 30,
      );
    return icon;
  }
}
