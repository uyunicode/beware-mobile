import 'package:beware_mobile_app/src/pages/login/widgets/background_login.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ForgotPasswordPage extends StatefulWidget {
  ForgotPasswordPage({Key? key}) : super(key: key);

  @override
  _ForgotPasswordPageState createState() => _ForgotPasswordPageState();
}

class _ForgotPasswordPageState extends State<ForgotPasswordPage> {
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    final _size = MediaQuery.of(context).size;
    return Scaffold(
        body: Stack(
      children: <Widget>[
        BackgroundLogin(),
        SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.fromLTRB(0, 80, 0, 40),
                child: SvgPicture.asset(
                  'assets/icons/forgot-password-amico.svg',
                  height: _size.height * 0.30,
                  width: _size.width * 0.30,
                ),
              ),
              Container(
                padding: EdgeInsets.fromLTRB(35, 0, 35, 0),
                alignment: Alignment.center,
                child: Text("danos tu email para el envio de una correo de seguridad y poder restaurar tu cuenta", style: TextStyle(color: Colors.deepPurple)),
              ),
              SizedBox(
                height: 10.0,
              ),
              Container(
                padding: EdgeInsets.fromLTRB(35, 0, 35, 0),
                child: Form(
                    key: _formKey,
                    child: Column(
                      children: <Widget>[
                        _userTextFormFieldEmail(),
                        SizedBox(
                          height: 15,
                        ),
                        _sendEmailSecurityButton(context)
                      ],
                    )),
              )
            ],
          ),
        )
      ],
    ));
  }

  InputDecoration _inputDecoration(String hintText, String labelText, Icon icon) {
    Widget _suffix = Text("");
    return InputDecoration(
        fillColor: Color.fromRGBO(249, 243, 255, 1.0),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20),
            borderSide: BorderSide(
              color: Color.fromRGBO(249, 243, 255, 1.0),
            )),
        focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20),
            borderSide: BorderSide(
              color: Color.fromRGBO(249, 255, 255, 1.0),
            )),
        filled: true,
        hintText: hintText,
        labelText: labelText,
        icon: icon,
        suffixIcon: _suffix);
  }

  Widget _userTextFormFieldEmail() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      cursorColor: Colors.deepPurple,
      style: TextStyle(
        color: Color.fromRGBO(89, 88, 90, 1.0),
        fontStyle: FontStyle.italic,
      ),
      decoration: _inputDecoration("Escribe tu email", "Email", Icon(Icons.alternate_email, color: Colors.deepPurple)),
      validator: (value) {
        if (value!.isEmpty) return 'Escribe tu email!';
      },
      onChanged: (value) {
        print(value);
      },
    );
  }

  Widget _sendEmailSecurityButton(BuildContext context) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(primary: Colors.deepPurple, onPrimary: Colors.white, shape: StadiumBorder(), fixedSize: Size(290, 40)),
      onPressed: () {
        print("clicked!!");
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [Icon(Icons.send_rounded), SizedBox(width: 5.0), Text("Enviar email de Seguridad")],
      ),
    );
  }
}
