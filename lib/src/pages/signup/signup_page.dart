import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:beware_mobile_app/src/services/HttpServer.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:beware_mobile_app/src/pages/login/login_validator.dart';
import 'package:beware_mobile_app/src/providers/user_provider.dart';
import 'package:beware_mobile_app/src/pages/login/widgets/background_login.dart';
import 'package:select_form_field/select_form_field.dart';

class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  final _formKey = GlobalKey<FormState>();
  String _password = "";

  bool _obscurePasswordState = true;

  @override
  Widget build(BuildContext context) {
    final UserProvider userProvider = Provider.of(context);
    final _size = MediaQuery.of(context).size;
    return Scaffold(
        body: Stack(
      children: <Widget>[
        BackgroundLogin(),
        SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Container(padding: EdgeInsets.fromLTRB(0, 80, 0, 40), child: Text("Sign Up")),
              Container(
                padding: EdgeInsets.fromLTRB(35, 0, 35, 0),
                child: Form(
                    key: _formKey,
                    child: Column(
                      children: <Widget>[
                        _userTextFormFieldCI(userProvider),
                        SizedBox(
                          height: 15,
                        ),
                        _userTextFormFieldName(userProvider),
                        SizedBox(
                          height: 15,
                        ),
                        _userTextFormFieldPhone(userProvider),
                        SizedBox(
                          height: 15,
                        ),
                        _userTextFormFieldEmail(userProvider),
                        SizedBox(
                          height: 15,
                        ),
                        _passwordTextFormField(),
                        SizedBox(
                          height: 15,
                        ),
                        _userTextFormFieldNickName(userProvider),
                        SizedBox(
                          height: 15,
                        ),
                        _userSelectFormFieldSex(userProvider),
                        SizedBox(
                          height: 15,
                        ),
                        _userTextFormFieldBirthday(userProvider),
                        SizedBox(
                          height: 15,
                        ),
                        _userSelectFormFieldCivilStatus(userProvider),
                        SizedBox(
                          height: 15,
                        ),
                        _userTextFormFieldAddress(userProvider),
                        SizedBox(
                          height: 15,
                        ),
                        _signupButton(context, userProvider)
                      ],
                    )),
              )
            ],
          ),
        )
      ],
    ));
  }

  Widget _signupButton(BuildContext context, UserProvider userProvider) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(primary: Colors.deepPurple, onPrimary: Colors.white, shape: StadiumBorder(), fixedSize: Size(290, 40)),
      onPressed: () => _signupRequest(context, userProvider),
      child: Text("Registrate"),
    );
  }

  _signupRequest(BuildContext context, UserProvider userProvider) async {
    if (_formKey.currentState!.validate()) {
      HttpServer httpServer = HttpServer();
      Map response = await httpServer.post("/user_management/possible_victim_manage/sign_up", {
        "ci": userProvider.ci,
        "name": userProvider.name,
        "cellphone": userProvider.cellphone,
        "email": userProvider.email,
        "password": _password,
        "state": true,
        "type_user": true,
        "nickname": userProvider.nickname,
        "sex": userProvider.sex,
        "birthday": userProvider.birthDay,
        "civil_status": userProvider.civilStatus,
        "address": userProvider.address,
        "profile": "profile.jpg"
      });

      if (response['status'] == 200) {
        await _setUserProvider(userProvider);
        Navigator.of(context).pushNamedAndRemoveUntil('home', (Route<dynamic> route) => true);
      } else if (response['status'] == 500)
        _showMessage(response['msg']);
      else
        _showMessage("Error, Intente nuevamente!");
    }
  }

  _setUserProvider(UserProvider userProvider) async {
    HttpServer httpServer = HttpServer();
    Map response = await httpServer.get("/user_management/possible_victim_manage/${userProvider.email}");
    if (response['status'] == 200) {
      response = response['data'];
      userProvider.setAll(response['ci'], response['name'], response['cellphone'], response['email'], response['nickname'], response['sex'], response['birthday'], response['civil_status'],
          response['address'], response['profile']);
    }
  }

  _showMessage(String message) {
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 1,
        backgroundColor: Color.fromRGBO(230, 230, 243, 1),
        textColor: Colors.deepPurple,
        fontSize: 16.0);
  }

  InputDecoration _inputDecoration(String hintText, String labelText, Icon icon) {
    Widget _suffix = Text("");
    return InputDecoration(
        fillColor: Color.fromRGBO(249, 243, 255, 1.0),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20),
            borderSide: BorderSide(
              color: Color.fromRGBO(249, 243, 255, 1.0),
            )),
        focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20),
            borderSide: BorderSide(
              color: Color.fromRGBO(249, 255, 255, 1.0),
            )),
        filled: true,
        hintText: hintText,
        labelText: labelText,
        icon: icon,
        suffixIcon: _suffix);
  }

  Widget _userTextFormFieldCI(UserProvider userProvider) {
    return TextFormField(
      // keyboardType: TextInputTyp,
      cursorColor: Colors.deepPurple,
      style: TextStyle(
        color: Color.fromRGBO(89, 88, 90, 1.0),
        fontStyle: FontStyle.italic,
      ),
      decoration: _inputDecoration("Escribe tu CI", "CI", Icon(Icons.perm_identity_sharp, color: Colors.deepPurple)),
      validator: (value) {
        if (value!.isEmpty) return 'Escribe tu CI!!';
      },
      onChanged: (value) {
        userProvider.ci = int.parse(value);
      },
    );
  }

  Widget _userTextFormFieldName(UserProvider userProvider) {
    return TextFormField(
      // keyboardType: TextInputTyp,
      cursorColor: Colors.deepPurple,
      style: TextStyle(
        color: Color.fromRGBO(89, 88, 90, 1.0),
        fontStyle: FontStyle.italic,
      ),
      decoration: _inputDecoration("Escribe tu nombre", "nombre", Icon(Icons.text_fields_outlined, color: Colors.deepPurple)),
      validator: (value) {
        if (value!.isEmpty) return 'Escribe tu nombre!!';
      },
      onChanged: (value) {
        userProvider.name = value;
      },
    );
  }

  Widget _userTextFormFieldPhone(UserProvider userProvider) {
    return TextFormField(
      // keyboardType: TextInputTyp,
      cursorColor: Colors.deepPurple,
      style: TextStyle(
        color: Color.fromRGBO(89, 88, 90, 1.0),
        fontStyle: FontStyle.italic,
      ),
      decoration: _inputDecoration("Escribe tu telefono", "telefono", Icon(Icons.phone, color: Colors.deepPurple)),
      validator: (value) {
        if (value!.isEmpty) return 'Escribe tu telefono!!';
      },
      onChanged: (value) {
        userProvider.cellphone = value;
      },
    );
  }

  Widget _userTextFormFieldNickName(UserProvider userProvider) {
    return TextFormField(
      // keyboardType: TextInputTyp,
      cursorColor: Colors.deepPurple,
      style: TextStyle(
        color: Color.fromRGBO(89, 88, 90, 1.0),
        fontStyle: FontStyle.italic,
      ),
      decoration: _inputDecoration("Escribe tu nickname", "nickname", Icon(Icons.adb, color: Colors.deepPurple)),
      validator: (value) {
        if (value!.isEmpty) return 'Escribe tu nickname!!';
      },
      onChanged: (value) {
        userProvider.nickname = value;
      },
    );
  }

  Widget _userTextFormFieldEmail(UserProvider userProvider) {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      cursorColor: Colors.deepPurple,
      style: TextStyle(
        color: Color.fromRGBO(89, 88, 90, 1.0),
        fontStyle: FontStyle.italic,
      ),
      decoration: _inputDecoration("Escribe tu email", "Email", Icon(Icons.alternate_email, color: Colors.deepPurple)),
      validator: (value) {
        if (value!.isEmpty) return 'Escribe tu email!';
      },
      onChanged: (value) {
        userProvider.email = value;
      },
    );
  }

  Widget _userSelectFormFieldSex(UserProvider userProvider) {
    return SelectFormField(
        type: SelectFormFieldType.dropdown, // or can be dialog
        cursorColor: Colors.deepPurple,
        style: TextStyle(
          color: Color.fromRGBO(89, 88, 90, 1.0),
          fontStyle: FontStyle.italic,
        ),
        initialValue: 'true',
        decoration: _inputDecoration("Escribe tu sexo", "sexo", Icon(Icons.person, color: Colors.deepPurple)),
        labelText: 'Shape',
        items: [
          {'value': true, 'label': 'Femenino'},
          {'value': false, 'label': 'Masculino'}
        ],
        onChanged: (value) {
          userProvider.sex = value.toLowerCase() == 'true';
        });
  }

  Widget _userTextFormFieldBirthday(UserProvider userProvider) {
    return TextFormField(
      keyboardType: TextInputType.datetime,
      cursorColor: Colors.deepPurple,
      style: TextStyle(
        color: Color.fromRGBO(89, 88, 90, 1.0),
        fontStyle: FontStyle.italic,
      ),
      decoration: _inputDecoration("Escribe tu cumpleaños", "Fecha de Nacimiento", Icon(Icons.calendar_today_outlined, color: Colors.deepPurple)),
      validator: (value) {
        if (value!.isEmpty) return 'Escribe tu cumpleaños!';
      },
      onChanged: (value) {
        userProvider.birthDay = value;
      },
    );
  }

  Widget _userSelectFormFieldCivilStatus(UserProvider userProvider) {
    return SelectFormField(
        type: SelectFormFieldType.dropdown, // or can be dialog
        cursorColor: Colors.deepPurple,
        style: TextStyle(
          color: Color.fromRGBO(89, 88, 90, 1.0),
          fontStyle: FontStyle.italic,
        ),
        initialValue: 'SOL',
        decoration: _inputDecoration("Escribe tu Estado Civil", "estado civil", Icon(Icons.person, color: Colors.deepPurple)),
        labelText: 'Estado Civil',
        items: [
          {'value': 'SOL', 'label': 'Soltero(a)'},
          {'value': 'CAS', 'label': 'Casado(a)'},
          {'value': 'DIV', 'label': 'Divorciado(a)'},
          {'value': 'VIU', 'label': 'Viudo(a)'},
          {'value': 'SEP', 'label': 'Separado(a)'}
        ],
        onChanged: (value) {
          userProvider.civilStatus = value;
        });
  }

  Widget _userTextFormFieldAddress(UserProvider userProvider) {
    return TextFormField(
      // keyboardType: TextInputType.datetime,
      cursorColor: Colors.deepPurple,
      style: TextStyle(
        color: Color.fromRGBO(89, 88, 90, 1.0),
        fontStyle: FontStyle.italic,
      ),
      decoration: _inputDecoration("Escribe tu Direccion", "Direccion", Icon(Icons.home_outlined, color: Colors.deepPurple)),
      validator: (value) {
        if (value!.isEmpty) return 'Escribe tu direccion!';
      },
      onChanged: (value) {
        userProvider.address = value;
      },
    );
  }

  Widget _passwordTextFormField() {
    return TextFormField(
      keyboardType: TextInputType.visiblePassword,
      obscureText: _obscurePasswordState,
      decoration: _inputDecoration("Escribe Tu Contraseña", "Contraseña", Icon(Icons.password, color: Colors.deepPurple)),
      validator: (value) {
        if (value!.isEmpty) return 'Escribe tu contraseña!';
        if (!LoginValidator.validatePassword(value)) return 'Contraseña Incorrecta';
      },
      onChanged: (value) {
        _password = value;
      },
    );
  }
}
