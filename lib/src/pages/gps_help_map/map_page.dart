import 'dart:async';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:beware_mobile_app/src/providers/socket_service.dart';
import 'package:provider/provider.dart';

class MapPage extends StatefulWidget {
  MapPage({Key? key}) : super(key: key);

  @override
  _MapPageState createState() => _MapPageState();
}

class _MapPageState extends State<MapPage> {
  Completer<GoogleMapController> _controller = Completer();
  Map<dynamic, dynamic> _victimUbication = {
    "latitude": -17.7839081,
    "longitude": -63.1860173
  };
  List<Marker> _markers = [
    new Marker(
        markerId: MarkerId("victim_id"),
        position: LatLng(-17.7839081, -63.1860173),
        icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet))
  ];
  SocketService? _socket;

  @override
  Widget build(BuildContext context) {
    this._socket = Provider.of<SocketService>(context);
    return Scaffold(
        body: GoogleMap(
      initialCameraPosition: CameraPosition(
        target: LatLng(this._victimUbication['latitude'],
            this._victimUbication['longitude']),
        zoom: 11,
      ),
      markers: this._markers.toSet(),
      mapType: MapType.terrain,
      myLocationEnabled: true,
      myLocationButtonEnabled: true,
      onMapCreated: (GoogleMapController controller) async {
        _controller.complete(controller);
        _listenServer();
        await _moveCamera();
      },
    ));
  }

  _moveCamera() async {
    final GoogleMapController controller = await _controller.future;
    final cameraPosition = new CameraPosition(
      target: LatLng(this._victimUbication['latitude'],
          this._victimUbication['longitude']),
      zoom: 11,
    );
    controller.animateCamera(CameraUpdate.newCameraPosition(cameraPosition));
  }

  _listenServer() {
    this._socket?.socket?.on('position', (data) {
      setState(() {
        this._victimUbication['latitude'] = data['data']['latitude'];
        this._victimUbication['longitude'] = data['data']['longitude'];
        this._markers[0] = new Marker(
            markerId: MarkerId("victim_id"),
            position: LatLng(this._victimUbication['latitude'],
                this._victimUbication['longitude']),
            icon: BitmapDescriptor.defaultMarkerWithHue(
                BitmapDescriptor.hueViolet));
      });
    });
  }
}
