import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';

class AccessGPSPage extends StatefulWidget {
  const AccessGPSPage({Key? key}) : super(key: key);

  @override
  _AccessGPSPageState createState() => _AccessGPSPageState();
}

class _AccessGPSPageState extends State<AccessGPSPage> with WidgetsBindingObserver {
  @override
  void initState() {
    // TODO: implement initState
    WidgetsBinding.instance?.addObserver(this);
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    WidgetsBinding.instance?.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async {
    if (state == AppLifecycleState.resumed) {
      if (await Permission.location.isGranted) {
        Navigator.pushReplacementNamed(context, 'loading');
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text("Para usar esta app dnecesita GPS"),
          MaterialButton(
              child: Text("Solicitar acceso", style: TextStyle(color: Colors.white)),
              color: Colors.deepPurple[900],
              shape: StadiumBorder(),
              elevation: 0,
              splashColor: Colors.transparent,
              onPressed: () async {
                // TODO: verificar permissions
                final status = await Permission.location.request();
                print("Permision GPS: " + status.toString());
                GPSAccess(status);
              })
        ],
      ),
    ));
  }

  void GPSAccess(PermissionStatus status) {
    switch (status) {
      case PermissionStatus.granted:
        // TODO: Handle this case.
        Navigator.pushReplacementNamed(context, 'map');
        break;
      case PermissionStatus.limited:
      case PermissionStatus.denied:
      case PermissionStatus.restricted:
      case PermissionStatus.permanentlyDenied:
        // TODO: Handle this case.
        openAppSettings();
        break;
    }
  }
}
