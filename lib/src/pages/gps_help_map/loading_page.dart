import 'package:beware_mobile_app/src/helpers/helpers.dart';
import 'package:beware_mobile_app/src/pages/gps_help_map/access_gps_page.dart';
import 'package:beware_mobile_app/src/pages/gps_help_map/map_page.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:permission_handler/permission_handler.dart';

class LoadingPage extends StatelessWidget {
  const LoadingPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder(
        future: this.checkGPSLocation(context),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.data) {
            return Center(child: Text(snapshot.data));
          } else {
            return Center(
              child: CircularProgressIndicator(strokeWidth: 3),
            );
          }
        },
      ),
    );
  }

  Future checkGPSLocation(BuildContext context) async {
    // TODO: Permission GPS
    final permissionGPS = await Permission.location.isGranted;
    final gpsActive = await Geolocator.isLocationServiceEnabled();
    if (gpsActive && permissionGPS) {
      Navigator.pushReplacement(context, navegateMapFadeIn(context, MapPage()));
      return '';
    } else if (!permissionGPS) {
      Navigator.pushReplacement(context, navegateMapFadeIn(context, AccessGPSPage()));
      return 'necesitamos tu permiso a GPS';
    } else {
      return 'Active el GPS';
    }
  }
}
