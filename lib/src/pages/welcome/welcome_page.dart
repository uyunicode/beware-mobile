import 'package:beware_mobile_app/src/pages/login/login_page.dart';
import 'package:flutter/material.dart';

class Welcome extends StatefulWidget {
  @override
  _WelcomeState createState() => _WelcomeState();
}

class _WelcomeState extends State<Welcome> {
  PageController _pageViewController = new PageController(initialPage: 0);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        controller: _pageViewController,
        scrollDirection: Axis.horizontal,
        children: [_welcome(context), Login()],
      ),
    );
  }

  Widget _welcome(BuildContext context) {
    return Stack(
      children: [
        _backgroundColor(),
        _backgroundImage(),
        _welcomeInfo(),
      ],
    );
  }

  Widget _welcomeInfo() {
    final textStyle = TextStyle(
        color: Colors.white,
        fontSize: 30.0,
        fontStyle: FontStyle.italic,
        fontWeight: FontWeight.bold);
    return SafeArea(
      child: Column(
        children: <Widget>[
          Text("Cuidate!", style: textStyle),
          Text("Bienvenido", style: textStyle),
          Expanded(child: Container()),
          TextButton.icon(
            onPressed: () => _nextPage(),
            label: Text(
              "Siguiente",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold,
                  fontStyle: FontStyle.italic),
            ),
            icon: Icon(Icons.keyboard_arrow_right,
                size: 70.0, color: Colors.white),
          )
        ],
      ),
    );
  }

  _nextPage() {
    _pageViewController.nextPage(
        duration: Duration(milliseconds: 800), curve: Curves.fastOutSlowIn);
  }

  Widget _backgroundColor() {
    return Container(
        width: double.infinity,
        height: double.infinity,
        color: Color.fromRGBO(108, 192, 218, 1.0));
  }

  Widget _backgroundImage() {
    return Container(
      width: double.infinity,
      height: double.infinity,
      child: Image(
        image: AssetImage('assets/images/socorro.jpg'),
        fit: BoxFit.cover,
      ),
    );
  }
}
