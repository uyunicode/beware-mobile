import 'package:advanced_search/advanced_search.dart';
import 'package:beware_mobile_app/src/pages/login/widgets/background_login.dart';
import 'package:beware_mobile_app/src/providers/trusted_contact.dart';
import 'package:beware_mobile_app/src/providers/trusted_person.dart';
import 'package:beware_mobile_app/src/providers/user_provider.dart';
import 'package:beware_mobile_app/src/services/HttpServer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';

class TrustedContactsPage extends StatefulWidget {
  TrustedContactsPage({Key? key}) : super(key: key);

  @override
  _TrustedContactsPageState createState() => _TrustedContactsPageState();
}

class _TrustedContactsPageState extends State<TrustedContactsPage> {
  List listPerson = [];
  List listContact = [];
  List listPossibleUsers = [];
  List<String> listPossibleUsersSearch = [];
  HttpServer http = new HttpServer();
  bool state = false;
  String nicknameSelected = "";
  String phoneContact = "";
  String emailContact = "";
  String nameContact = "";

  Future<void> _getRequestHTTP(TrustedContactProvider trustedContacts, TrustedPersonProvider trustedPerson, UserProvider user) async {
    print("initialize request-----------");
    Map datas = await http.get("/user_management/possible_victim_manage/get_list_trusted_person/${user.ci}");
    var val = datas['data']['data'];
    trustedPerson.setlistPersonOfUser(val);
    this.listPerson = val;
    datas = await http.get("/user_management/possible_victim_manage/get_list_trusted_contact/${user.ci}");
    val = datas['data'];
    trustedContacts.setListContact(val);
    this.listContact = val;
    datas = await http.get("/user_management/possible_victim_manage/get_lists/${user.ci}");
    this.listPossibleUsers = datas['data'];
    print("end request-----------");
    for (var i = 0; i < this.listPossibleUsers.length; i++) {
      this.listPossibleUsersSearch.add(this.listPossibleUsers[i]["nickname"]);
    }
    // print(datas["data"]);
    // print("end END-----------");
  }

  @override
  Widget build(BuildContext context) {
    final _size = MediaQuery.of(context).size;
    final TrustedPersonProvider trustedPerson = Provider.of(context);
    final TrustedContactProvider trustedContact = Provider.of(context);
    final UserProvider user = Provider.of(context);
    if (!state) {
      this._getRequestHTTP(trustedContact, trustedPerson, user);
      setState(() {
        state = true;
      });
    }
    return Scaffold(
        body: Stack(
          alignment: Alignment.topCenter,
          children: [
            BackgroundLogin(),
            SingleChildScrollView(
              physics: ScrollPhysics(),
              child: Column(
                children: [
                  Container(
                    // padding: EdgeInsets.fromLTRB(0, 80, 0, 40),
                    child: SvgPicture.asset(
                      'assets/icons/cuate_solidarity.svg',
                      height: _size.height * 0.30,
                      width: _size.width * 0.30,
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(30, 0, 30, 0),
                    child: Column(
                      children: <Widget>[
                        Text(
                          'Gestionar Amigos de Confianza',
                          style: TextStyle(color: Colors.deepPurple, fontSize: 18.0),
                        ),
                        Text("Agrega a tus amigos de confianza aqui."),
                        SizedBox(
                          height: 10,
                        ),
                        _getInputSearchFriends(),
                        SizedBox(
                          height: 10,
                        ),
                        _getButtonAddFriendList(user),
                        Text(
                          'Lista de amigos de confianza',
                          style: TextStyle(color: Colors.deepPurple, fontSize: 18.0),
                        ),
                        _getListFriends(user),
                        Text(
                          'Lista de contactos externos',
                          style: TextStyle(color: Colors.deepPurple, fontSize: 18.0),
                        ),
                        _getListContacts(user),
                      ],
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
        floatingActionButton: getAddContactButton(user));
  }

  Widget _getInputSearchFriends() {
    return SafeArea(
      child: AdvancedSearch(
        data: this.listPossibleUsersSearch,
        maxElementsToDisplay: 2,
        singleItemHeight: 50,
        borderColor: Colors.deepPurple,
        minLettersForSearch: 0,
        selectedTextColor: Colors.deepPurple,
        fontSize: 14,
        borderRadius: 12.0,
        hintText: 'Busca a tus amigos de confianza',
        autoCorrect: false,
        focusedBorderColor: Colors.deepPurple,
        enabledBorderColor: Colors.black,
        enabled: true,
        caseSensitive: false,
        inputTextFieldBgColor: Colors.white10,
        clearSearchEnabled: true,
        itemsShownAtStart: 2,
        searchMode: SearchMode.CONTAINS,
        showListOfResults: true,
        unSelectedTextColor: Colors.black54,
        verticalPadding: 10,
        horizontalPadding: 10,
        hideHintOnTextInputFocus: true,
        hintTextColor: Colors.grey,
        onItemTap: (index, value) {
          print("selected item Index is $index");
        },
        onSearchClear: () {
          print("Cleared Search");
        },
        onSubmitted: (value, value2) {
          print("Submitted: " + value);
        },
        onEditingProgress: (value, value2) async {
          print("TextEdited: " + value);
          print("current: " + value2.toString());
          print("LENGTH: " + value2.length.toString());
          await this._registerTrustedPerson(value);
        },
      ),
    );
  }

  _registerTrustedPerson(String nickname) async {
    for (var i = 0; i < this.listPossibleUsers.length; i++) {
      if (this.listPossibleUsers[i]['nickname'] == nickname) {
        this.nicknameSelected = nickname;
        return;
      }
    }
    _showMessage(nickname + " no existe !!");
  }

  _showMessage(String message) {
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 1,
        backgroundColor: Color.fromRGBO(230, 230, 243, 1),
        textColor: Colors.deepPurple,
        fontSize: 16.0);
  }

  Widget _getButtonAddFriendList(UserProvider userProvider) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(primary: Colors.deepPurple, onPrimary: Colors.white, shape: StadiumBorder(), fixedSize: Size(290, 40)),
      onPressed: () async {
        for (var i = 0; i < this.listPossibleUsers.length; i++) {
          if (this.listPossibleUsers[i]['nickname'] == this.nicknameSelected) {
            Map response = await http.post('/user_management/trusted_person_manage/add_trusted_person', {"ci_victim": userProvider.ci, "ci_person_trusted": this.listPossibleUsers[i]["ci_victim"]});
            if (response["status"] == 200) {
              _showMessage(this.nicknameSelected + " agregado correctamente");
            } else {
              _showMessage("no se pudo agregar " + this.nicknameSelected + " intente nuevamente porfavor");
            }
            return;
          }
        }
        _showMessage(this.nicknameSelected + " no existe !!");
      },
      child: Text("AGREGAR A LISTA DE AMIGO"),
    );
  }

  Widget _getListFriends(UserProvider userProvider) {
    return ListView.builder(
      itemCount: this.listPerson.length,
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemBuilder: (context, index) => _getSlidableWithLists(userProvider, this.listPerson[index], context, index),
    );
  }

  Widget _getSlidableWithLists(UserProvider userProvider, var list, BuildContext context, int index) {
    return Slidable(
        key: ValueKey(index),
        actionPane: SlidableDrawerActionPane(),
        child: Card(
            borderOnForeground: true,
            shadowColor: Colors.deepPurple,
            key: UniqueKey(),
            child: ListTile(
              leading: Icon(Icons.verified_user, color: Colors.green),
              title: Text(
                list["name"] + " (" + list["nickname"] + ")",
                style: TextStyle(
                  fontSize: 12,
                  color: Colors.deepPurple,
                ),
              ),
              trailing: Icon(
                Icons.chevron_right,
                color: Colors.deepPurple,
              ),
            )),
        secondaryActions: <Widget>[
          IconSlideAction(
            caption: "eliminar",
            color: Colors.red,
            icon: Icons.delete_outline_sharp,
            onTap: () async {
              for (var i = 0; i < this.listPossibleUsers.length; i++) {
                if (this.listPossibleUsers[i]['nickname'] == list["nickname"]) {
                  Map response =
                      await http.post('/user_management/trusted_person_manage/remove_trusted_person', {"ci_victim": userProvider.ci, "ci_person_trusted": this.listPossibleUsers[i]["ci_victim"]});
                  if (response["status"] == 200) {
                    _showMessage(list["nickname"] + " se eliminó correctamente");
                  } else {
                    _showMessage("no se pudo eliminar " + list["nickname"] + " intente nuevamente porfavor");
                  }
                  return;
                }
              }
              _showMessage(list["nickname"] + " no existe !!");
            },
          )
        ]);
  }

  InputDecoration _inputDecoration(String hintText, String labelText, Icon icon) {
    Widget _suffix = Text("");
    return InputDecoration(
        fillColor: Color.fromRGBO(249, 243, 255, 1.0),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(30),
        ),
        enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20),
            borderSide: BorderSide(
              color: Color.fromRGBO(249, 243, 255, 1.0),
            )),
        focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20),
            borderSide: BorderSide(
              color: Color.fromRGBO(249, 255, 255, 1.0),
            )),
        filled: true,
        hintText: hintText,
        labelText: labelText,
        icon: icon,
        suffixIcon: _suffix);
  }

  Widget getAddContactButton(UserProvider userProvider) {
    return FloatingActionButton(
      onPressed: () {
        // Add your onPressed code here!
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                content: Stack(
                  children: <Widget>[
                    Positioned(
                      right: -50.0,
                      top: -50.0,
                      child: InkResponse(
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                        child: CircleAvatar(
                          child: Icon(Icons.close),
                          backgroundColor: Colors.red,
                        ),
                      ),
                    ),
                    Form(
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          TextFormField(
                            cursorColor: Colors.deepPurple,
                            style: TextStyle(
                              color: Color.fromRGBO(89, 88, 90, 1.0),
                              fontStyle: FontStyle.italic,
                            ),
                            decoration: _inputDecoration("Escribe nro telefonico", "Telefono", Icon(Icons.phone_android_outlined, color: Colors.deepPurple)),
                            validator: (value) {
                              if (value!.isEmpty) return 'Escribe el telefono!!';
                            },
                            onChanged: (value) {
                              setState(() {
                                this.phoneContact = value;
                              });
                            },
                          ),
                          TextFormField(
                            // keyboardType: TextInputTyp,
                            cursorColor: Colors.deepPurple,
                            style: TextStyle(
                              color: Color.fromRGBO(89, 88, 90, 1.0),
                              fontStyle: FontStyle.italic,
                            ),
                            decoration: _inputDecoration("Escribe tu email", "email", Icon(Icons.email_outlined, color: Colors.deepPurple)),
                            validator: (value) {
                              if (value!.isEmpty) return 'Escribe tu email!!';
                            },
                            onChanged: (value) {
                              setState(() {
                                this.emailContact = value;
                              });
                            },
                          ),
                          TextFormField(
                            // keyboardType: TextInputTyp,
                            cursorColor: Colors.deepPurple,
                            style: TextStyle(
                              color: Color.fromRGBO(89, 88, 90, 1.0),
                              fontStyle: FontStyle.italic,
                            ),
                            decoration: _inputDecoration("Escribe tu nombre", "Nombre", Icon(Icons.emoji_people, color: Colors.deepPurple)),
                            validator: (value) {
                              if (value!.isEmpty) return 'Escribe tu nombre!!';
                            },
                            onChanged: (value) {
                              setState(() {
                                this.nameContact = value;
                              });
                            },
                          ),
                          ElevatedButton(
                            style: ElevatedButton.styleFrom(primary: Colors.deepPurple, onPrimary: Colors.white, shape: StadiumBorder(), fixedSize: Size(290, 40)),
                            onPressed: () async {
                              Map response = await http.post('/user_management/contact_manage/create_contact', {"cellphone": this.phoneContact, "email": this.emailContact, "name": this.emailContact});
                              if (response["status"] == 200) {
                                _showMessage(this.phoneContact + " se creo correctamente");
                                response =
                                    await http.post('/user_management/trusted_person_manage/add_trusted_contact', {"ci_victim": userProvider.ci, "cellphone_trusted_contact": this.phoneContact});
                                if (response["status"] == 200) {
                                  _showMessage(this.phoneContact + " se agrego a su lista de contactos");
                                } else {
                                  _showMessage("no se pudo crear" + this.phoneContact + " intente nuevamente porfavor");
                                }
                              } else {
                                _showMessage("no se pudo crear" + this.phoneContact + " intente nuevamente porfavor");
                              }
                            },
                            child: Text("Agregar Contacto"),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              );
            });
      },
      child: const Icon(Icons.contact_phone),
      backgroundColor: Colors.deepPurple,
    );
  }

  Widget _getListContacts(UserProvider userProvider) {
    return ListView.builder(
      itemCount: this.listContact.length,
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemBuilder: (context, index) => _getSlidableWithListsContact(userProvider, this.listContact[index], context, index),
    );
  }

  _getSlidableWithListsContact(UserProvider userProvider, Map listContact, BuildContext context, int index) {
    return Slidable(
        key: ValueKey(index),
        actionPane: SlidableDrawerActionPane(),
        child: Card(
            borderOnForeground: true,
            shadowColor: Colors.deepPurple,
            key: UniqueKey(),
            child: ListTile(
              leading: Icon(Icons.verified_user, color: Colors.green),
              title: Text(
                listContact["cellphone"] + " (" + listContact["name"] + ")",
                style: TextStyle(
                  fontSize: 12,
                  color: Colors.deepPurple,
                ),
              ),
              trailing: Icon(
                Icons.chevron_right,
                color: Colors.deepPurple,
              ),
            )),
        secondaryActions: <Widget>[
          IconSlideAction(
            caption: "eliminar",
            color: Colors.red,
            icon: Icons.delete_outline_sharp,
            onTap: () async {
              Map response = await http.post('/user_management/trusted_person_manage/remove_trusted_contact', {"ci_victim": userProvider.ci, "cellphone_trusted_contact": listContact["cellphone"]});
              if (response["status"] == 200) {
                _showMessage(listContact["cellphone"] + " se elimino de su lista de contactos");
              } else {
                _showMessage("no se pudo eliminar" + listContact["cellphone"] + " intente nuevamente porfavor");
              }
            },
          )
        ]);
  }
}
