import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:beware_mobile_app/src/providers/trusted_person.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:io';

import 'package:fluttertoast/fluttertoast.dart';
import 'package:geolocator/geolocator.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'package:rhino/rhino_error.dart';
import 'package:rhino/rhino_manager.dart';

import 'package:beware_mobile_app/src/pages/login/widgets/background_login.dart';
import 'package:beware_mobile_app/src/services/Geolocator.dart';
import 'package:beware_mobile_app/src/services/HttpServer.dart';
import 'package:beware_mobile_app/src/providers/socket_service.dart';
import 'package:beware_mobile_app/src/providers/user_provider.dart';

class BodyHome extends StatefulWidget {
  BodyHome({Key? key}) : super(key: key);

  @override
  _BodyHomeState createState() => _BodyHomeState();
}

class _BodyHomeState extends State<BodyHome> with WidgetsBindingObserver {
  RhinoManager? _rhinoManager;
  String _keyWord = "gato"; //it's going to change
  SocketService? _socket;
  UserProvider _userProvider = new UserProvider();
  TrustedPersonProvider _trustedPersonProvider = new TrustedPersonProvider();
  HttpServer _http = new HttpServer();
  int _requestCode = 0;
  bool _isFirstPhoto = true; //if false is the 1st time to take a picture

  @override
  void initState() {
    super.initState();
    _initRhino();
    this._userProvider = Provider.of<UserProvider>(context, listen: false);
    this._trustedPersonProvider =
        Provider.of<TrustedPersonProvider>(context, listen: false);
    this._socket = Provider.of<SocketService>(context, listen: false);
    _listenServer();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        BackgroundLogin(),
        Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                width: 200,
                height: 200,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('assets/gif/tapme.gif'),
                        fit: BoxFit.cover)),
                child: _buttonSendDistressNotification(),
              ),
              _buttonTakePhoto()
            ],
          ),
        )
      ],
    );
  }

  Widget _buttonSendDistressNotification() {
    return TextButton(
      child: Text(
        "SOS",
        style: TextStyle(
            color: Colors.red,
            fontSize: 30.0,
            fontWeight: FontWeight.bold,
            fontStyle: FontStyle.italic),
      ),
      onPressed: () async => await _sendDistressNotification(),
    );
  }

  Widget _buttonTakePhoto() {
    return TextButton.icon(
      icon: Icon(
        Icons.camera_alt,
        color: Colors.deepPurple,
        size: 35.0,
      ),
      label: Text("Tomar Fotografía",
          style: TextStyle(
            color: Colors.deepPurple,
            fontSize: 16,
            fontStyle: FontStyle.italic,
          )),
      onPressed: () async => await _takePhoto(),
    );
  }

  Future<void> _sendDistressNotification() async {
    var currentPosition = await GeolocatorService.determinePosition();
    Map response = await _http.post(
        '/request_help_management/request_help_manage/create_request_help', {
      'date_time': DateTime.now().toString(),
      'initial_location':
          "${currentPosition.latitude}, ${currentPosition.longitude}",
      'filename1': "a.jpg",
      'filename2': "b.jpg",
      'ci_victim': _userProvider.ci
    });
    if (response["status"] == 200) {
      this._requestCode = response["data"];
      this._showMessage(
          "Enviando Notificación a mis contactos con mi ubicación en tiempo real....");
      List<int> ciListPersonOfUsers = await this._getCiListPersonOfUser();
      _emitNotification(ciListPersonOfUsers);
      _emitPositionStream(ciListPersonOfUsers);
    } else {
      this._showMessage("Error al enviar notificación");
    }
  }

  void _emitPositionStream(List<int> ciListPersonOfUsers) {
    Geolocator.getPositionStream(
            desiredAccuracy: LocationAccuracy.high,
            distanceFilter: 1,
            forceAndroidLocationManager: true)
        .listen((Position position) {
      print(
          "latitude: ${position.latitude}\n Longitude: ${position.longitude}");
      this._socket?.socket?.emit('position', {
        'from': _userProvider.ci,
        'to': ciListPersonOfUsers,
        'data': {'latitude': position.latitude, 'longitude': position.longitude}
      });
    });
  }

  void _emitNotification(List<int> ciListPersonOfUsers) {
    this._socket?.socket?.emit('notification', {
      'from': _userProvider.ci,
      'to': ciListPersonOfUsers,
      'data': {
        'title': "${_userProvider.name}: Ayuda!",
        'body': "necesito de tu ayuda, me encuentro en una situación de peligro"
      }
    });
  }

  Future<List<int>> _getCiListPersonOfUser() async {
    List<int> ciList = [];
    List listPersonOfUser = [];
    listPersonOfUser = this._trustedPersonProvider.getListPersonOfUser();
    Map response =
        await _http.get("/user_management/police_manage/list_police_user");
    for (var person in listPersonOfUser) {
      ciList.add(person['ci']);
    }
    listPersonOfUser = response['data'];
    for (var police in listPersonOfUser) {
      ciList.add(police['ci']);
    }
    return ciList;
  }

  _listenServer() {
    this._socket?.socket?.on('position', (data) {
      print("Position: $data"); //this data is send to socket service
    });
    this._socket?.socket?.on('notification', (data) {
      print("Notification: $data"); //this data is send to socket service
      Notify(data);
    });
  }

  Future<void> _takePhoto() async {
    final ImagePicker _picker = ImagePicker();
    final XFile? photo = await _picker.pickImage(source: ImageSource.camera);
    File photoFile = new File("${photo?.path}");
    if (this._requestCode > 0) {
      int fileNumber = 1;
      if (!this._isFirstPhoto) fileNumber = 2;
      var formData = new FormData.fromMap({
        'code': this._requestCode,
        'numberFile': fileNumber,
        'image': await MultipartFile.fromFile(photoFile.path)
      });
      Map response = await this._http.post(
          "/request_help_management/request_help_manage/save_image_request_help",
          formData);
      if (response['status'] == 200) {
        this._isFirstPhoto = false;
        this._showMessage("Fotografía enviada exitosamente!");
      } else
        this._showMessage("Fotografía NO enviada, intente nuevamente");
    } else {
      this._showMessage("Primero de hacer un pedido de auxilio!");
    }
  }

  ////////////////////////////////////////////////////////////////
  ///Rhino process
  Future<void> _initRhino() async {
    String platform = Platform.isAndroid
        ? "android"
        : Platform.isIOS
            ? "ios"
            : throw new PvError("This demo supports iOS and Android only.");
    String contextPath =
        "assets/rhino/contexts/$platform/es_${platform}_2021-08-31.rhn";

    try {
      _rhinoManager = await RhinoManager.create(contextPath, inferenceCallback,
          modelPath: "assets/rhino/models/rhino_params_es.pv",
          sensitivity: .30,
          errorCallback: errorCallback);
      await _startProcessing();
    } on PvError catch (ex) {
      print("Failed to initialize Rhino: ${ex.message}");
    }
  }

  void inferenceCallback(Map<String, dynamic> inference) async {
    bool isUnderstood = inference['isUnderstood'];
    if (isUnderstood) {
      String intent = inference['intent'];
      switch (intent) {
        case 'start':
          await _startProcessing();
          break;
        case 'send':
          Map<String, String> slots = inference['slots'];
          String? optionItem = slots['optionsItem'];
          if (optionItem == _keyWord) await _sendDistressNotification();
          break;
        case 'take':
          await this._takePhoto();
          break;
        case 'stop':
          _stopProcessing();
          break;
        default:
      }
    }
  }

  void errorCallback(PvError error) {
    print(error.message);
  }

  Future<void> _startProcessing() async {
    try {
      await _rhinoManager!.process();
    } on PvAudioException catch (ex) {
      print("Failed to start audio capture: ${ex.message}");
    }
  }

  _stopProcessing() {}

  void Notify(data) async {
    String timezone = await AwesomeNotifications().getLocalTimeZoneIdentifier();
    await AwesomeNotifications().createNotification(
      content: NotificationContent(
          id: 1,
          channelKey: 'key1',
          title: '${data["title"]}',
          body: '${_userProvider.name}, ${data["body"]}'),
      // schedule: NotificationInterval(interval: 5, timeZone: timezone, repeats: false)
    );
  }

  void _showMessage(String message) {
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 1,
        backgroundColor: Color.fromRGBO(230, 230, 243, 1),
        textColor: Colors.deepPurple,
        fontSize: 16.0);
  }
}
