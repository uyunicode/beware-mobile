import 'dart:math';

import 'package:beware_mobile_app/src/pages/home/widgets/trusted_contacts_page.dart';
import 'package:beware_mobile_app/src/pages/intrafamily_violence_test/intrafamily_violence_test_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';

import 'package:beware_mobile_app/src/pages/home/widgets/body_home_page.dart';
import 'package:beware_mobile_app/src/providers/user_provider.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  double value = 0;
  int _selectedIndex = 0;
  List<Widget> _listPages = <Widget>[
    BodyHome(),
    IntrafamilyViolenceTest(),
    TrustedContactsPage(),
  ];

  @override
  Widget build(BuildContext context) {
    final UserProvider userProvider = Provider.of(context);
    final _size = MediaQuery.of(context).size;
    return Scaffold(
        body: Stack(
      children: [
        Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(colors: [
            Colors.deepPurple.shade400,
            Colors.deepPurple.shade800
          ], begin: Alignment.bottomCenter, end: Alignment.topCenter)),
        ),
        SafeArea(
            child: Container(
          width: 200.0,
          padding: EdgeInsets.all(8.0),
          child: Column(
            children: [
              DrawerHeader(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    CircleAvatar(
                      radius: 50.0,
                      backgroundImage: NetworkImage(
                          "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS3Fbmz98uGL5zx0dPHLVFmOaBRO46-YJo33kmAtW7zNHoj30XFGi2J0zKzqVWXTDlj8Bc&usqp=CAU"),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    Text("Eren Jager",
                        style: TextStyle(color: Colors.white, fontSize: 20.0))
                  ],
                ),
              ),
              Expanded(
                child: ListView(
                  children: [
                    ListTile(
                      onTap: () {},
                      leading: Icon(Icons.home, color: Colors.white),
                      title:
                          Text("Home", style: TextStyle(color: Colors.white)),
                    ),
                    ListTile(
                      onTap: () {
                        Navigator.of(context).pushNamedAndRemoveUntil(
                            'request_list', (Route<dynamic> route) => true);
                      },
                      leading: Icon(Icons.list, color: Colors.white),
                      title: Text("Lista Solicitudes de Ayuda",
                          style: TextStyle(color: Colors.white)),
                    ),
                    ListTile(
                      onTap: () {},
                      leading: Icon(Icons.settings, color: Colors.white),
                      title: Text("Settings",
                          style: TextStyle(color: Colors.white)),
                    ),
                    ListTile(
                      onTap: () {},
                      leading: Icon(Icons.logout, color: Colors.white),
                      title:
                          Text("Logout", style: TextStyle(color: Colors.white)),
                    )
                  ],
                ),
              ),
              Container(
                child: SvgPicture.asset(
                  'assets/icons/world_day_against_trafficking_in_persons_rafiki.svg',
                  height: _size.height * 0.30,
                  width: _size.width * 0.30,
                ),
              )
            ],
          ),
        )),
        TweenAnimationBuilder(
            tween: Tween<double>(begin: 0, end: value),
            duration: Duration(milliseconds: 500),
            curve: Curves.easeInExpo,
            builder: (_, double val, __) {
              return (Transform(
                alignment: Alignment.center,
                transform: Matrix4.identity()
                  ..setEntry(3, 2, 0.001)
                  ..setEntry(0, 3, 200 * val)
                  ..rotateY((pi / 6) * val),
                child: Scaffold(
                  body: Center(child: _listPages.elementAt(_selectedIndex)),
                  bottomNavigationBar: BottomNavigationBar(
                    backgroundColor: Color.fromRGBO(249, 243, 255, 1.0),
                    currentIndex: _selectedIndex,
                    selectedItemColor: Colors.deepPurple,
                    items: <BottomNavigationBarItem>[
                      BottomNavigationBarItem(
                        icon: Icon(Icons.location_history),
                        label: "SOS",
                      ),
                      BottomNavigationBarItem(
                          icon: Icon(Icons.question_answer), label: "Test"),
                      BottomNavigationBarItem(
                          icon: Icon(Icons.group_add_rounded),
                          label: "Contactos"),
                    ],
                    onTap: (index) {
                      setState(() {
                        _selectedIndex = index;
                      });
                    },
                  ),
                ),
              ));
            }),
        GestureDetector(
          onHorizontalDragUpdate: (details) {
            if (details.delta.dx > 0) {
              setState(() {
                value = 1;
              });
            } else {
              setState(() {
                value = 0;
              });
            }
          },
        )
      ],
    ));
  }
}
