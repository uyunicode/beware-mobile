import 'package:beware_mobile_app/src/pages/login/widgets/background_login.dart';
import 'package:beware_mobile_app/src/providers/request_help_list.dart';
import 'package:beware_mobile_app/src/providers/user_provider.dart';
import 'package:beware_mobile_app/src/services/HttpServer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';

class RequestHelpListPage extends StatefulWidget {
  const RequestHelpListPage({Key? key}) : super(key: key);

  @override
  _RequestHelpListPageState createState() => _RequestHelpListPageState();
}

class _RequestHelpListPageState extends State<RequestHelpListPage> {
  List<dynamic> listRequestHelp = [];
  final scrollController = ScrollController();
  HttpServer http = HttpServer();
  final itemSize = 70.0;
  bool switchRequest = false;

  void onListen() {
    setState(() {});
  }

  @override
  void initState() {
    // TODO: implement initState
    scrollController.addListener(onListen);
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    scrollController.removeListener(onListen);
    super.dispose();
  }

  Future<void> getListRequestHelp(RequestHelpListProvider requestLiostProvider, int ciPossibleVictim) async {
    final Map response = await http.get("/request_help_management/request_help_manage/get_request_help_list/" + ciPossibleVictim.toString());
    if (response["status"] == 200) {
      this.listRequestHelp = response["data"];
      requestLiostProvider.requestHelpList = response["data"];
      setState(() {});
    } else {
      Fluttertoast.showToast(
          msg: "Por ahora no hay Lista de solicitudes",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 2,
          backgroundColor: Color.fromRGBO(230, 230, 243, 1),
          textColor: Colors.deepPurple,
          fontSize: 16.0);
    }
  }

  Future<Null> getRefresh(RequestHelpListProvider requestHelpListProvider, int ciPossibleVictim) async {
    await getListRequestHelp(requestHelpListProvider, ciPossibleVictim);
    await Future.delayed(Duration(seconds: 3));
  }

  @override
  Widget build(BuildContext context) {
    UserProvider userProvider = Provider.of<UserProvider>(context);
    RequestHelpListProvider requestListPorvider = Provider.of<RequestHelpListProvider>(context);
    if (!switchRequest) {
      this.getListRequestHelp(requestListPorvider, userProvider.ci);
      setState(() {
        switchRequest = true;
      });
    }
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          title: Text("Lista de Pedidos de Ayuda", style: TextStyle(color: Colors.deepPurple)),
          centerTitle: true,
          elevation: 0,
        ),
        body: Padding(
            padding: const EdgeInsets.all(15.0),
            child: RefreshIndicator(
                onRefresh: () async {
                  await getRefresh(requestListPorvider, userProvider.ci);
                },
                backgroundColor: Colors.deepPurple,
                color: Colors.white,
                child: ListView.builder(
                    itemCount: listRequestHelp.length,
                    itemBuilder: (context, index) {
                      var request = listRequestHelp[index];
                      int countFilesUploadedRequest = request['filename1'].length > 0 ? 1 : 0;
                      countFilesUploadedRequest = request['filename2'].length > 0 ? countFilesUploadedRequest + 1 : countFilesUploadedRequest;
                      return Slidable(
                          key: ValueKey(index),
                          actionPane: SlidableDrawerActionPane(),
                          child: Card(
                            key: Key(request['code'].toString()),
                            shadowColor: Colors.deepPurple,
                            color: Colors.deepPurple,
                            elevation: 6, // Change this
                            child: ListTile(
                                leading: Text(request['code'].toString(), style: TextStyle(color: Colors.white, fontSize: 16.0)),
                                title: Text(request['date_request'], style: TextStyle(color: Colors.white, fontSize: 15.0)),
                                subtitle: Text('localización: ${request['initial_location']} files: $countFilesUploadedRequest', style: TextStyle(color: Colors.white, fontSize: 12.0)),
                                trailing: Icon(Icons.location_on_sharp, color: Colors.white)),
                          ),
                          secondaryActions: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: IconSlideAction(
                                icon: Icons.map,
                                caption: "Ver Mapa",
                                color: Colors.green,
                                onTap: () {
                                  print(request['code'].toString());
                                  Navigator.of(context).pushNamedAndRemoveUntil('map', (Route<dynamic> route) => true);
                                },
                              ),
                            )
                          ]);
                    }))));
  }
}
