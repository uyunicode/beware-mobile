import 'package:flutter/foundation.dart';

class RequestHelpListProvider extends ChangeNotifier {
  List requestHelpList;

  RequestHelpListProvider() : this.requestHelpList = new List.empty();

  setAll() {
    this.requestHelpList = requestHelpList;
  }

  List get requestHelpListOfUser {
    return this.requestHelpList;
  }

  void setRequestHelpList(List requestListHelper) {
    this.requestHelpList = requestListHelper;
    notifyListeners();
  }
}
