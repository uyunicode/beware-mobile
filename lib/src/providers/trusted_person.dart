import 'package:flutter/foundation.dart';

class TrustedPersonProvider extends ChangeNotifier {
  List listPerson = List.empty();

  TrustedPersonProvider() : this.listPerson = new List.empty();

  setAll() {
    this.listPerson = listPerson;
  }

  List getListPersonOfUser() {
    return this.listPerson;
  }

  void setlistPersonOfUser(List trustedPerson) {
    this.listPerson = trustedPerson;
    notifyListeners();
  }
}
