import 'package:flutter/foundation.dart';

class TrustedContactProvider extends ChangeNotifier {
  List listContacts;

  TrustedContactProvider() : this.listContacts = new List.empty();

  setAll() {
    this.listContacts = listContacts;
  }

  List get listContactsOfUser {
    return this.listContacts;
  }

  void setListContact(List trustedContacs) {
    this.listContacts = trustedContacs;
    notifyListeners();
  }
}
