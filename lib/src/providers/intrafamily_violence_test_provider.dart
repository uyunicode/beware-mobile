import 'package:flutter/material.dart';

class IntrafamilyViolenceTestProvider extends ChangeNotifier {
  List _questions = [];
  List<int> _answers = []; // 0: nunca, 1: en ocasiones, 2: casi siempre

  IntrafamilyViolenceTestProvider() {
    for (int i = 0; i < 33; i++) {
      this.answers.add(1);
    }
  }
  List get questions => this._questions;

  List<int> get answers => this._answers;

  set questions(List question) {
    this._questions = question;
    notifyListeners();
  }

  set answers(List<int> answers) {
    this._answers = answers;
    notifyListeners();
  }

  setAnswer(int values, int position) {
    this.answers[position] = values;
    notifyListeners();
  }
}
