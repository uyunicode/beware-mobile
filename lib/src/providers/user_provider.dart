import 'package:flutter/material.dart';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class UserProvider extends ChangeNotifier {
  int _ci;
  String _name;
  String _cellphone;
  String _email;
  String _nickname;
  bool _sex;
  String _birthDay;
  String _civilStatus;
  String _address;
  String _profile;

  UserProvider()
      : _ci = 0,
        _name = "",
        _cellphone = "",
        _email = "",
        _nickname = "",
        _sex = true,
        _birthDay = "",
        _civilStatus = "",
        _address = "",
        _profile = "";

  setAll(
    int ci,
    String name,
    String cellphone,
    String email,
    String nickname,
    bool sex,
    String birthDay,
    String civilStatus,
    String address,
    String profile,
  ) {
    this.ci = ci;
    this.name = name;
    this.cellphone = cellphone;
    this.email = email;
    this.nickname = nickname;
    this.sex = sex;
    this.birthDay = birthDay;
    this.civilStatus = civilStatus;
    this.address = address;
    this.profile = profile;
  }

  int get ci => _ci;

  String get name => _name;

  String get cellphone => _cellphone;

  String get email => _email;

  String get nickname => _nickname;

  bool get sex => _sex;

  String get birthDay => _birthDay;

  String get civilStatus => _civilStatus;

  String get address => _address;

  String get profile => _profile;

  set ci(int ci) {
    _ci = ci;
    notifyListeners();
  }

  set name(String name) {
    _name = name;
    notifyListeners();
  }

  set cellphone(String cellphone) {
    _cellphone = cellphone;
    notifyListeners();
  }

  set email(String email) {
    _email = email;
    notifyListeners();
  }

  set nickname(String nickname) {
    _nickname = nickname;
    notifyListeners();
  }

  set sex(bool sex) {
    _sex = sex;
    notifyListeners();
  }

  set birthDay(String birthDay) {
    _birthDay = birthDay;
    notifyListeners();
  }

  set civilStatus(String civilStatus) {
    _civilStatus = civilStatus;
    notifyListeners();
  }

  set address(String address) {
    _address = address;
    notifyListeners();
  }

  set profile(String profile) {
    _profile = profile;
    notifyListeners();
  }

  static Future<String?> getToken() async {
    final storage = new FlutterSecureStorage();
    final token = await storage.read(key: 'token');
    return token;
  }

  static Future<void> writeToken(String? token) async {
    final storage = new FlutterSecureStorage();
    await storage.write(key: 'token', value: token);
  }

  static Future<void> deleteToken() async {
    final storage = new FlutterSecureStorage();
    await storage.delete(key: 'token');
  }
}
