import 'package:beware_mobile_app/src/providers/user_provider.dart';
import 'package:flutter/material.dart';
import "package:socket_io_client/socket_io_client.dart" as IO;

enum ServerStatus { Online, Offline, Connecting }

class SocketService with ChangeNotifier {
  IO.Socket? _socket;
  ServerStatus _serverStatus = ServerStatus.Offline;

  ServerStatus get serverStatus => this._serverStatus;
  IO.Socket? get socket => this._socket;

  Future<void> connect() async {
    final token = await UserProvider.getToken();
    this._socket = IO.io(
        "http://ec2-3-21-164-145.us-east-2.compute.amazonaws.com:3000",
        IO.OptionBuilder()
            .setTransports(["websocket"])
            .disableAutoConnect()
            .setExtraHeaders({'x-access-token': token})
            .build());
    this._serverStatus = ServerStatus.Offline;
    this._socket!.connect();
    this._socket!.onConnect((_) {
      this._serverStatus = ServerStatus.Online;
      notifyListeners();
      print("conected to server");
    });
    this._socket!.onDisconnect((_) {
      this._serverStatus = ServerStatus.Offline;
      notifyListeners();
      print("disconnected from server");
    });
  }

  void disconnect() {
    this._socket!.disconnect();
    this._socket!.onDisconnect((_) {
      this._serverStatus = ServerStatus.Offline;
      notifyListeners();
      print("disconnected from server");
    });
  }
}
