import 'package:beware_mobile_app/src/pages/forgot_password/forgot_password._page.dart';
import 'package:beware_mobile_app/src/pages/gps_help_map/access_gps_page.dart';
import 'package:beware_mobile_app/src/pages/gps_help_map/loading_page.dart';
import 'package:beware_mobile_app/src/pages/gps_help_map/map_page.dart';
import 'package:beware_mobile_app/src/pages/request_help/request_help_list_page.dart';
import 'package:beware_mobile_app/src/pages/signup/signup_page.dart';
import 'package:beware_mobile_app/src/pages/welcome/welcome_page.dart';
import 'package:beware_mobile_app/src/providers/intrafamily_violence_test_provider.dart';
import 'package:beware_mobile_app/src/providers/request_help_list.dart';
import 'package:beware_mobile_app/src/providers/socket_service.dart';
import 'package:beware_mobile_app/src/providers/trusted_contact.dart';
import 'package:beware_mobile_app/src/providers/trusted_person.dart';
import 'package:beware_mobile_app/src/providers/user_provider.dart';
import 'package:flutter/material.dart';

import 'package:beware_mobile_app/src/pages/home/home_page.dart';
import 'package:beware_mobile_app/src/pages/login/login_page.dart';
import 'package:provider/provider.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => UserProvider()),
        ChangeNotifierProvider(create: (_) => TrustedPersonProvider()),
        ChangeNotifierProvider(create: (_) => TrustedContactProvider()),
        ChangeNotifierProvider(create: (_) => SocketService()),
        ChangeNotifierProvider(create: (_) => RequestHelpListProvider()),
        ChangeNotifierProvider(create: (_) => IntrafamilyViolenceTestProvider())
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Cuidate',
        initialRoute: 'welcome',
        routes: {
          'welcome': (context) => Welcome(),
          'login': (context) => Login(),
          'home': (context) => Home(),
          'signup': (context) => SignUp(),
          'request_list': (context) => RequestHelpListPage(),
          'map': (context) => MapPage(),
          'loading': (context) => LoadingPage(),
          'access_gps': (context) => AccessGPSPage(),
          'forgot_password': (context) => ForgotPasswordPage(),
        },
      ),
    );
  }
}
