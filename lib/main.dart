import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:flutter/material.dart';
import 'app.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  AwesomeNotifications().initialize(null, [
    NotificationChannel(
        channelKey: 'key1',
        channelName: 'Beware Software',
        channelDescription: 'solicitud de Auxilio',
        enableVibration: true,
        defaultColor: Colors.deepPurple,
        ledColor: Colors.white,
        playSound: true,
        enableLights: true)
  ]);
  runApp(MyApp());
}
